
// Basic arithmetic operations
const myLib = {

    /**
     * Addition function
     * @param {*} a 
     * @param {*} b 
     * @returns {number}
     */
    add: (a, b) => {
        return a + b;
    },

    /**
     * Subtraction function
     * @param {*} a 
     * @param {*} b 
     * @returns {number}
     */
    subtract: (a, b) => {
        return a - b;
    },

    /**
     * Division function
     * @param {*} dividend 
     * @param {*} divisor 
     * @returns {number}
     * @throws {Error} if divisor is 0
     */
    divide: (dividend, divisor) => {
        if (divisor === 0) {
            throw new Error('Cannot divide by 0');
        }
        return dividend / divisor;
    },

    /**
     * {function}
     * @param {*} a 
     * @param {*} b 
     * @returns {number}
     */
    multiply: function(a, b) {
        return a * b;
    }
}

module.exports = myLib;
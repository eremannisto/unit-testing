const myLib = require('./mylib');

console.log("Addition: 1 + 2 = " + myLib.add(1, 2));
console.log("Subtraction: 2 - 1 = " + myLib.subtract(2, 1));
console.log("Division: 4 / 2 = " + myLib.divide(4, 2));
console.log("Multiplication: 2 * 2 = " + myLib.multiply(2, 2));
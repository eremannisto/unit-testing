Basic arithmetic operations
==========================

This is a simple library for basic arithmetic operations.

Available operations
--------------------

* Addition
* Subtraction
* Multiplication
* Division


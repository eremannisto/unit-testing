const expect = require('chai').expect;
const myLib = require('../src/mylib');


describe("Basic arithmetic operations", () => {

    before(() => {

        // Inirialize
        // Create ab object
        console.log("Initializing calculator...\n");

    });

    it("Can add 1 and 2 together", () => {
        expect(myLib.add(1, 2)).to.equal(3, "1 + 2 should equal 3");
    });

    it("Can subtract 2 from 1", () => {
        expect(myLib.subtract(1, 2)).to.equal(-1, "1 - 2 should equal -1");
    });

    it("Can divide 4 by 2", () => {
        expect(myLib.divide(4, 2)).to.equal(2 , "4 / 2 should equal 2");
    }); 

    it("Cannot divide by 0", () => {
        expect(() => myLib.divide(4, 0)).to.throw(Error, "Cannot divide by 0");
    });

    it("Can multiply 2 by 2", () => {
        expect(myLib.multiply(2, 2)).to.equal(4 , "2 * 2 should equal 4");
    });

    after(() => {
        // Clean up
        // For example, shut down a Express server
        console.log("\nTestings completed");
    });
});